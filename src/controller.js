/**
 * Window manager controller
 */
"user strict";
import Events from "events";
//
export default class Controller extends Events {
    constructor(__config) {
        super();
        this.config = __config;
        this.keyEventController = new KeyEventController(__config);
        this.renderController = new RenderController(__config);
        this.layoutController = new LayoutController(__config);
    }

    add(__options) {
        let options = new WindowOptions(__options);
        let win = new Window(options);
        win.addListenner("key", this.onWindowKeyEvent.bind(this));
        win.addListenner("layout", this.onWindowLayoutEvent.bind(this));
        win.addListenner("render", this.onWindowRenderEvent());
    }

    onKeyevent(__event) {
        let event = this.keyEventcontroller.onEvent(__event);
    }

    onRenderEvent(__event){
        let event = this.renderController.onEvent(__event);
        let windows =   this.layoutConteroller.getRenderables();
        windows.forEach(__window => {
            __window.onRender(event);
        }
    }

    getRenderWindows(){
        this.layoutConteroller.
    }
}
